package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class MathTest {

    @Test
    public void factorialTest()
    {
        Math username = new Math();
        assertEquals(username.factorial(5), 120);
    }

    @Test
    public void factorial2Test()
    {
        Math username = new Math();
        assertEquals(username.factorial2(5), 120);
    }
}
